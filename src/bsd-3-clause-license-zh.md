> This is an unofficial translation of the 3-Clause BSD License into Simplified Chinese. It does not legally state the distribution terms for software that uses the 3-Clause BSD License—only the original English text of the 3-Clause BSD License does that. However, we hope that this translation will help Simplified Chinese speakers understand the 3-Clause BSD License better. This translation is released into the public domain under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/).

> 这是 3-Clause BSD 许可证的非正式简体中文翻译。它不是使用 3-Clause BSD 许可证软件的发布法律声明——只有 3-Clause BSD 许可证的原始英文版才有法律意义。不过，我们希望该翻译能够帮助简体中文用户更好地理解 3-Clause BSD 许可证。此翻译文本以[CC0 1.0 通用](https://creativecommons.org/licenses/by/4.0/)发布至公有领域。

---

# 3-Clause BSD 许可证
版权所有 <年份> <版权持有人>

只要满足以下条件，则允许再分发及使用源形式和二进制形式，无论其修改与否：

1. 源代码的再分发必须保留上述版权声明、本处所列条件及以下免责声明。
2. 二进制形式的再分发必须将上述版权声明、本处所列条件及以下免责声明复制到随着该分发一起提供的文档和/或其他材料中。
3. 未经事先明确的书面许可，不得使用版权持有人之名或其贡献者之名来宣传或推广本软件的衍生产品。

版权持有人及其贡献者“照原样”提供本软件，且拒绝提供任何显示或隐式的担保，包括但不限于有经济价值和适合特定用途的保证。在任何情况下，版权持有人或贡献者都不对任何直接的、间接的、偶发的、特殊的、严重的或必然的损害（包括但不限于采购替代品或服务器；可用性、数据或利润损失；或业务中断）负责，无论其起因为何，也无论追责的形式如何，即无论该要求来自合同、严格的责任还是任何因使用此软件而造成（包括出于疏忽或其他原因）的侵权行为，即使已告知此类损害的可能性。
