# 自由软件许可证翻译

此页面列出了一些流行的[自由软件](https://www.gnu.org/philosophy/free-sw.html)或[“开源软件”](https://www.gnu.org/philosophy/open-source-misses-the-point.html)许可证的中文翻译文本。整合此译文列表，旨在帮助中文使用者更好地理解某一许可证的内容。请注意，此页面列出的许可证翻译仅用于理解用途，不应作为具有实际法律效力的许可证原文的替代。更多关于此页面的信息见页尾。

> 本项目已不再活跃维护。请转至由 OAF 活跃维护的许可证翻译项目[“源译识”](https://atomgit.com/translation/license-translation)。

# 本人的翻译

## GNU General Public License, version 3

> ### [GNU 通用公共许可证](gplv3-zh.html)
>
> - 译者：Peaksol
> - 首次发布：2022/03/12
> - 上次修改：2023/09/04
> - 译文许可证：[位于 gnu.org 的条款](https://www.gnu.org/licenses/translations.html)

## GNU Affero General Public License, version 3

> ### [GNU Affero 通用公共许可证](agplv3-zh.html)
>
> - 译者：Peaksol
> - 首次发布：2022/06/06
> - 上次修改：2023/09/04
> - 译文许可证：[位于 gnu.org 的条款](https://www.gnu.org/licenses/translations.html)

## GNU Lesser General Public License, version 3

> ### [GNU 宽通用公共许可证](lgplv3-zh.html)
>
> - 译者：Peaksol
> - 首次发布：2022/06/06
> - 上次修改：2023/09/04
> - 译文许可证：[位于 gnu.org 的条款](https://www.gnu.org/licenses/translations.html)

## GNU Free Documentation License, version 1.3

> ### [GNU 自由文档许可证](fdlv1.3-zh.html)
>
> - 译者：Peaksol
> - 首次发布：2022/12/10
> - 上次修改：2023/10/02
> - 译文许可证：[位于 gnu.org 的条款](https://www.gnu.org/licenses/translations.html)

## BSD 3-Clause License

> ### [3-Clause BSD 许可证](bsd-3-clause-license-zh.html)
>
> - 译者：Peaksol
> - 首次发布：2022/06/06
> - 上次修改：2022/06/06
> - 译文版权：公有领域（[CC0](https://creativecommons.org/publicdomain/zero/1.0/)）

## Expat License

> ### [MIT 许可证](mit-license-zh.html)
>
> - 译者：Peaksol
> - 首次发布：2022/06/06
> - 上次修改：2023/02/06
> - 译文版权：公有领域（[CC0](https://creativecommons.org/publicdomain/zero/1.0/)）

# 其他翻译
## GNU General Public License, version 3

> ### [GNU 通用公共许可协议](https://jxself.org/translations/gpl-3.zh.shtml)
>
> - 译者：阮坤良
> - 首次发布：未知
> - 上次修改：2017/07/05
> - 翻译质量：高
> - 译文许可证：[位于 gnu.org 的条款](https://www.gnu.org/licenses/translations.html)

> ### [GNU 通用公共授权](http://bergwolf.github.io/2007/06/29/gplv3-zh-cn.html)
>
> - 译者：Bergwolf
> - 首次发布：未知
> - 上次修改：2007/06/29
> - 翻译质量：中
> - 译文许可证：[CC BY-ND 2.0](http://creativecommons.org/licenses/by-nd/2.0/)

## GNU General Public License, version 2

> ### [GNU 通用公共许可协议](https://jxself.org/translations/gpl-2.zh.shtml)
>
> - 译者：阮坤良
> - 首次发布：未知
> - 上次修改：2017/06/08
> - 译文许可证：[位于 gnu.org 的条款](https://www.gnu.org/licenses/translations.html)

> ### [GNU 通用公共授权](http://www.thebigfly.com/gnu/gpl/)
>
> - 译者：Leo
> - 首次发布：未知
> - 上次修改：未知
> - 译文许可证：[位于 gnu.org 的条款](https://www.gnu.org/licenses/translations.html)

## GNU Affero General Public License, version 3

> ### [GNU Affero 通用公共许可协议](https://www.chinasona.org/gnu/agpl-3.0-cn.html)
>
> - 译者：Samuel Chong
> - 首次发布：未知
> - 上次修改：未知
> - 译文许可证：[位于 gnu.org 的条款](https://www.gnu.org/licenses/translations.html)

## GNU Lesser General Public License, version 3

> ### [GNU 较宽松公共许可证](http://www.thebigfly.com/gnu/lgpl/lgpl-v3.php)
>
> - 译者：Leo
> - 首次发布：未知
> - 上次修改：未知
> - 译文许可证：未知

## GNU Lesser General Public License, version 2.1

> ### [GNU 较宽松公共许可证](http://www.thebigfly.com/gnu/lgpl/)
>
> - 译者：Leo
> - 首次发布：未知
> - 上次修改：未知
> - 译文许可证：未知

## Apache License, version 2.0

> ### [Apache 许可证](https://mp.weixin.qq.com/s/bS1NvUUpmurMubZl80uumQ)
>
> - 译者：卫剑钒
> - 首次发布：2020/05/14
> - 译文许可证：CC BY

> ### [Apache 许可证](https://mp.weixin.qq.com/s/bS1NvUUpmurMubZl80uumQ)
>
> - 译者：程佳璇
> - 首次发布：2022/08/31
> - 译文许可证：未知

> ### [Apache 许可证](https://blog.csdn.net/arui319/article/details/2436097)
> 
> - 译者：Joe （hbW 改动）
> - 首次发布：2008/05/12
> - 译文许可证：未知

## Expat License

> ### [MIT 许可证](https://linux.cn/article-13180-1.html)
>
> - 译者：白宦成
> - 首次发布：2021/03/07
> - 译文版权：公有领域

# 关于本页面

![CC0](https://i.creativecommons.org/p/zero/1.0/88x31.png)

本列表由 Peaksol 维护，且根据 [CC0](https://creativecommons.org/publicdomain/zero/1.0/) 置于公有领域。

本项目的源 Markdown 文件可以在[此处](https://codeberg.org/Peaksol/licenses-translated)获取。
