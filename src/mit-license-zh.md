> This is an unofficial translation of the MIT License into Simplified Chinese. It does not legally state the distribution terms for software that uses the MIT License—only the original English text of the MIT License does that. However, we hope that this translation will help Simplified Chinese speakers understand the MIT License better. This translation is released into the public domain under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/).

> 这是 MIT 许可证的非正式简体中文翻译。它不是使用 MIT 许可证软件的发布法律声明——只有 MIT 许可证的原始英文版才有法律意义。不过，我们希望该翻译能够帮助简体中文用户更好地理解 MIT 许可证。此翻译文本以[CC0 1.0 通用](https://creativecommons.org/licenses/by/4.0/)发布至公有领域。

---

# MIT 许可证
版权所有 <年份> <版权持有人>

特此，为任何获得此软件及其相关文档文件的部分（下称“本软件”）的人，免费授予不受限制地处置本软件的权利，包括但不限于对本软件的使用、修改、合并、发布、分发、分授权和/或售卖其副本的权利，以及向被供以本软件的人授予上述权利的权利，前提是满足以下条件：

上述版权声明及本许可证声明应被包含于本软件的所有部分或主要部分之中。

本软件“照原样”发布，不附带任何形式的显示或隐式的担保，包括但不限于有经济价值、适合特定用途、无侵权行为的担保。在任何情况下，作者或版权持有人都不对任何索赔、损害或其他责任负责，无论其来自合同的起诉、侵权行为或其他来源，还是产生于、源于或有关于本软件或对其进行的其他处置。
