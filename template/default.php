<head>
	<title><?= basename($basename, '.md') ?> - 自由软件许可证翻译</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<style>
		html {
			background-color: #e4e4e4;
			padding: 0;
		}

		body {
			max-width: 74.92em;
			margin: auto;
			line-height: 1.5em;
			background-color: white;
			font-family: sans-serif;
			border: .1em solid #bbb;
			border-top: 0;
			box-shadow: 0 0 5px 5px #bbbbbb;
		}

		a {
			color: #049;
		}

		#top {
			border-top: 3px solid #a32d2a;
		}

		#content {
			padding: 1em 3%;
			margin: 0 auto;
			max-width: 48em;
		}

		blockquote {
			background: #f2f2f2;
			border: .1em solid #bbb;
			padding: 0 1.2em;
		}
		
		hr {
			clear: both;
			height: 2px;
			margin: 1.5em 0;
			border: 0;
			background: #bbb;
			display: block;
			color: #999;
		}

		.center {
			text-align: center;
		}

		h1 {
			color: #333333;
			font-size: 1.5em;
		}

		h1:first-of-type, h1:first-of-type + p {
			text-align: center;
		}

		h2 {
			color: #505050;
			font-size: 1.25em;
		}

		h3:not(blockquote h3) {
			font-weight: normal;
			font-style: italic;
			font-size: 1.125em;
		}

		pre {
			overflow: auto;
		}

		footer {
			font-size: .875em;
			padding: 1.5em 3%;
			color: #333;
			background: #f4f4f4;
			border-top: 3px solid #999;
		}

		footer p {
			margin: 0;
		}
	</style>
</head>

<body>
	<div id="top"></div>
	<div id="content">
		<?= $parsed ?>
	</div>
	<footer id="footer">
		<p>Published by Peaksol. General inquiries, corrections and suggestions can be sent to <code>&lt;p AT peaksol DOT org&gt;</code>.</p>
	</footer>
</body>
