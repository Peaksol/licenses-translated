# 自由软件许可证翻译

本仓库存储了所有本人翻译的自由软件许可证，包括 GNU GPL、AGPL、LGPL、FDL、BSD 3-Clause License 和 MIT License；同时还收集了一些社区已有的许可证译本。

本项目的 Markdown 源文件位于 `src` 目录，使用 [peaksol-org-ssg](https://codeberg.org/Peaksol/peaksol-org-ssg) 来生成 HTML 页面。可前往 [licenses.peaksol.org](https://licenses.peaksol.org/) 查看在线版。
